package main

import (
	"github.com/jessevdk/go-flags"
	"github.com/joho/godotenv"
	"github.com/ms-opa-bundle/config"
	"github.com/ms-opa-bundle/handler"
	"github.com/ms-opa-bundle/router"
	"github.com/ms-opa-bundle/service"
	log "github.com/sirupsen/logrus"
)

func main() {
	var err error
	log.SetFormatter(&log.JSONFormatter{})

	_, err = flags.Parse(&config.Opts)
	if err != nil {
		panic(err)
	}

	initEnvVars()
	config.LoadConfig()

	r := router.New()
	v1 := r.Group(config.Conf.RootPath)

	bundleService := service.NewBundleService()

	h := handler.NewHandler(bundleService)
	h.Register(v1)
	r.Logger.Fatal(r.Start(":" + config.Conf.Port))
}

func initEnvVars() {
	if godotenv.Load("profiles/default.env") != nil {
		log.Fatal("Error in loading environment variables from: profiles/default.env")
	} else {
		log.Info("Environment variables loaded from: profiles/default.env")
	}

	if config.Opts.Profile != "default" {
		profileFileName := "profiles/" + config.Opts.Profile + ".env"
		if godotenv.Overload(profileFileName) != nil {
			log.Fatal("Error in loading environment variables from: ", profileFileName)
		} else {
			log.Info("Environment variables overloaded from: ", profileFileName)
		}
	}
}
