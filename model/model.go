package model

// CustomerUser ...
type CustomerUser struct {
	CustomerID string  `json:"customerId"`
	Users      []int64 `json:"users"`
	SuperUsers []int64 `json:"superUsers"`
}
