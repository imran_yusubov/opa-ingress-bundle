package client

import (
	"github.com/ms-opa-bundle/config"
	"io/ioutil"

	"net/http"
)

// IUsersClient ...
type IUsersClient interface {
	GetAllCustomersAndUsers() ([]byte, error)
	GetCustomerPermissionData() ([]byte, error)
	GetAllPaymentTypes() ([]byte, error)
}

// UsersClient ...
type UsersClient struct {

}

// GetAllCustomersAndUsers ...
func (c *UsersClient) GetAllCustomersAndUsers() ([]byte, error) {
	resp, err := http.Get(config.Conf.UsersClientURL + "/v1/internal/users/customers-users/")
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if err != nil {
		return nil, err
	}

	return body, nil
}

// GetCustomerPermissionData ...
func (c *UsersClient) GetCustomerPermissionData() ([]byte, error) {
	resp, err := http.Get(config.Conf.UsersClientURL + "/v1/internal/users/permission-data/")
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if err != nil {
		return nil, err
	}

	return body, nil
}

// GetAllPaymentTypes ...
func (c *UsersClient) GetAllPaymentTypes() ([]byte, error) {
	resp, err := http.Get(config.Conf.UsersClientURL + "/v1/internal/users/payment-types/")
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if err != nil {
		return nil, err
	}

	return body, nil
}
