package service

import (
	"bytes"
	"github.com/ms-opa-bundle/client"
	"github.com/ms-opa-bundle/util"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"strings"
)

// IBundleService ...
type IBundleService interface {
	GetBundle() (*bytes.Buffer, error)
}

// BundleService ...
type BundleService struct {

}

// NewBundleService ...
func NewBundleService() *BundleService {
	return &BundleService{}
}

// GetBundle ...
func (bs *BundleService) GetBundle() (*bytes.Buffer, error) {
	log.Info("GetBundle.start")
	var PolicyFiles []string

	policyFiles, err := ioutil.ReadDir("policies")
	if err != nil {
		log.Error("GetBundle.error list policy files failed ", err)
		return nil, err
	}

	for _, f := range policyFiles {
		if strings.HasSuffix(f.Name(), ".rego") {
			PolicyFiles = append(PolicyFiles, "policies/" + f.Name())
		}
	}

	if len(PolicyFiles) == 0 {
		log.Error("GetBundle.error no policy files found in directory: policies")
		return nil, err
	}

	dataFiles := map[string][]byte{}

	for _, f := range PolicyFiles {
		data, err := ioutil.ReadFile(f)
		if err != nil {
			log.Error("GetBundle.error couldn't read policy file, ", err)
			return nil, err
		}

		dataFiles[f] = data
	}

	// Get customers-users data
	customerUsers, err := bs.getAllCustomersAndUsers()
	if err != nil {
		log.Error("GetBundle.error getAllCustomersAndUsers failed  ", err)
		return nil, err
	}
	dataFiles["/customers_users/data.json"] = customerUsers

	// Get customer payment permission data
	customerPermissionData, err := bs.getCustomerPermissionData()
	if err != nil {
		log.Error("GetBundle.error getCustomerPermissionData failed  ", err)
		return nil, err
	}
	dataFiles["/customer_payments/data.json"] = customerPermissionData

	// Get payment types
	paymentTypes, err := bs.getPaymentTypes()
	if err != nil {
		log.Error("GetBundle.error getPaymentTypes failed  ", err)
		return nil, err
	}
	dataFiles["/payment_types/data.json"] = paymentTypes

	buf := util.WriteTarGz(dataFiles)

	log.Info("GetBundle.success")
	return buf, nil
}

func (bs *BundleService) getAllCustomersAndUsers() ([]byte, error) {
	log.Info("getAllCustomersAndUsers.start")

	var usersClient = client.UsersClient{}
	customerUsers, err := usersClient.GetAllCustomersAndUsers()
	if err != nil {
		log.Error("getAllCustomersAndUsers.error usersClient failed  ", err)
		return nil, err
	}

	log.Info("getAllCustomersAndUsers.success")
	return customerUsers, nil
}

func (bs *BundleService) getCustomerPermissionData() ([]byte, error) {
	log.Info("getCustomerPermissionData.start")

	var usersClient = client.UsersClient{}
	customerPermissionData, err := usersClient.GetCustomerPermissionData()
	if err != nil {
		log.Error("getCustomerPermissionData.error usersClient failed  ", err)
		return nil, err
	}

	log.Info("getCustomerPermissionData.success")
	return customerPermissionData, nil
}

func (bs *BundleService) getPaymentTypes() ([]byte, error) {
	log.Info("getPaymentTypes.start")

	var usersClient = client.UsersClient{}
	paymentTypes, err := usersClient.GetAllPaymentTypes()
	if err != nil {
		log.Error("getPaymentTypes.error usersClient failed  ", err)
		return nil, err
	}

	log.Info("getPaymentTypes.success")
	return paymentTypes, nil
}