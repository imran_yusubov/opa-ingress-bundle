package config

import "github.com/alexflint/go-arg"

// Opts ...
var Opts struct {
	Profile string `short:"p" long:"profile" default:"dev" description:"Application run profile"`
}

// Config ...
type Config struct {
	Port           string `arg:"env:PORT,required"`
	RootPath       string `arg:"env:ROOT_PATH,required"`
	UsersClientURL string `arg:"env:MS_USERS,required"`
}

// Conf ...
var Conf Config

// LoadConfig ...
func LoadConfig() {
	arg.MustParse(&Conf)
}
