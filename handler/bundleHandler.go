package handler

import (
	"github.com/labstack/echo/v4"
	"net/http"
)

// GetBundle ...
func (h *Handler) GetBundle(c echo.Context) error {
	bundle, err := h.BundleService.GetBundle()
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	_, err = bundle.WriteTo(c.Response())
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusOK, bundle)
}