package handler

import "github.com/ms-opa-bundle/service"

// Handler ...
type Handler struct {
	*service.BundleService
}

// NewHandler ...
func NewHandler(bs *service.BundleService) *Handler {
	return &Handler{bs}
}
