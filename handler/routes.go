package handler

import (
	"github.com/labstack/echo/v4"
)

// Register ...
func (h *Handler) Register(v1 *echo.Group) {
	bundles := v1.Group("")
	bundles.GET("/bundle.tar.gz", h.GetBundle)

	health := v1.Group("/health")
	health.GET("", h.Health)

	readiness := v1.Group("/readiness")
	readiness.GET("", h.Health)
}
