package user.customer

import data.customers_users

default exists = false

exists {
	customers_users[i].customer_id == input.customer_id
	customers_users[i].users[j] == input.user_id
}
