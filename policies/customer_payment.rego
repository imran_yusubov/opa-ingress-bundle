package user.payment

import data.customer_payments
import data.payment_types

default create = false
default view = false

create {
	get_create_permission(input)
}

view {
	get_view_permission(input)
}

get_create_permission(request) {
	account := get_account(request)
	payment_types[_] == request.payment_type
	not is_not_permitted_type(account.not_permitted_create_types, request.payment_type)
}

get_view_permission(request) {
	account := get_account(request)
	payment_types[_] == request.payment_type
	not is_not_permitted_type(account.not_permitted_view_types, request.payment_type)
}

is_not_permitted_type(not_permitted_types, payment_type) {
	not_permitted_types[_] == payment_type
}

get_account(request) = account {
	customer := customer_payments[_]
	customer.id == request.customer_id

	user := customer.users[_]
	user.id == request.user_id

	account := user.accounts[_]
	account.account_no == request.account_no
}