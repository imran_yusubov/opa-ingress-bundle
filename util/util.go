package util

import (
	"archive/tar"
	"bytes"
	"compress/gzip"
	"strings"
	log "github.com/sirupsen/logrus"
)

// WriteTarGz ...
func WriteTarGz(files map[string][]byte) *bytes.Buffer {
	var buf bytes.Buffer
	gw := gzip.NewWriter(&buf)
	defer gw.Close()
	tw := tar.NewWriter(gw)
	defer tw.Close()

	for f, d := range files {
		if err := writeFile(tw, f, d); err != nil {
			log.Error(err)
		}
	}

	return &buf
}

func writeFile(tw *tar.Writer, path string, bs []byte) error {
	hdr := &tar.Header{
		Name:     "/" + strings.TrimLeft(path, "/"),
		Mode:     0600,
		Typeflag: tar.TypeReg,
		Size:     int64(len(bs)),
	}

	if err := tw.WriteHeader(hdr); err != nil {
		return err
	}

	_, err := tw.Write(bs)
	return err
}
